package org.coursera.fourth.rater;

import org.coursera.entities.Rating;
import org.coursera.rater.Rater;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EfficientRater implements Rater {
    private final String id;
    private final Map<String, Rating> ratingMap;

    public EfficientRater(String id) {
        this.id = id;
        ratingMap = new HashMap<>();
    }
    @Override
    public void addRating(String item, double rating) {
        ratingMap.put(item, new Rating(item, rating));
    }
    @Override
    public boolean hasRating(String item) {
        return ratingMap.containsKey(item);
    }

    @Override
    public String getId() {
        return this.id;
    }

    public double getRating(String item) {
        if (ratingMap.containsKey(item)) {
            return ratingMap.get(item).getValue();
        }
        return -1;
    }

    public int numRatings() {
        return ratingMap.size();
    }

    public ArrayList<String> getItemsRated() {
       return new ArrayList<>(ratingMap.keySet());
    }
}
