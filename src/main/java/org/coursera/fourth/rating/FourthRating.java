package org.coursera.fourth.rating;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.coursera.entities.Rating;
import org.coursera.fourth.db.MovieDatabase;
import org.coursera.fourth.db.RaterDatabase;
import org.coursera.fourth.filter.Filter;
import org.coursera.rater.Rater;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FourthRating {
    private static final Logger logger = LogManager.getLogger();

    private double dotProduct(Rater me, Rater r) {
        double dp = 0;
        List<String> movieId = me.getItemsRated();
        for (String id : movieId) {
            if (r.getItemsRated().contains(id)) {
                dp += (me.getRating(id) - 5) * (r.getRating(id) - 5);
            }
        }
        return dp;
    }

    private List<Rating> getSimilarities(String raterId) {
        List<Rating> simiList = new ArrayList<>();
        List<Rater> raters = RaterDatabase.getRaters();
        for (Rater r : raters) {
            if (!r.getId().equals(raterId)) {
                double dotProduct = dotProduct(RaterDatabase.getRater(raterId), r);
                if (dotProduct > 0) {
                    simiList.add(new Rating(r.getId(), dotProduct));
                }
            }
        }
        Collections.sort(simiList);
        Collections.reverse(simiList);
        return simiList;
    }

    public List<Rating> getSimilarRatings(String raterId, int numSimilarRaters, int minimalRaters) {
        List<Rating> ratingList = new ArrayList<>();
        List<String> movieIdByTopSimilar = new ArrayList<>();
        List<Rating> simiList1 = getSimilarities(raterId);
        for (int i = 0; i < numSimilarRaters; i++) {
            String raterID1 = simiList1.get(i).getItem();
            List<String> movieRated1 = RaterDatabase.getRater(raterID1).getItemsRated();
            for (String movieID : movieRated1) {
                if (!movieIdByTopSimilar.contains(movieID)) {
                    movieIdByTopSimilar.add(movieID);
                }
            }
        }
        for (String j : movieIdByTopSimilar) {
            double ave = 0;
            List<Rating> simiList = getSimilarities(raterId);
            int count = 0;
            double total = 0;
            int simiweighttotal = 0;
            for (int i = 0; i < numSimilarRaters; i++) {
                double rating = RaterDatabase.getRater(simiList.get(i).getItem()).getRating(j);
                if (rating != -1) {
                    count++;
                    total += rating * simiList.get(i).getValue();
                    simiweighttotal += simiList.get(i).getValue();
                }
            }
            if (count >= minimalRaters)
                ave = total / simiweighttotal;
            if (ave > 0)
                ratingList.add(new Rating(j, ave));
        }
        Collections.sort(ratingList);
        Collections.reverse(ratingList);

        return ratingList;
    }

    public List<Rating> getSimilarRatingsByFilter(String raterId, int numSimilarRaters, int minimalRaters, Filter f) {
        List<Rating> ratingList = new ArrayList<>();
        List<String> movieIdByTopSimilar = new ArrayList<>();
        List<Rating> simiList1 = getSimilarities(raterId);
        for (int i = 0; i < numSimilarRaters; i++) {
            String raterID1 = simiList1.get(i).getItem();
            List<String> movieRated1 = RaterDatabase.getRater(raterID1).getItemsRated();
            for (String movieID : movieRated1) {
                if (!movieIdByTopSimilar.contains(movieID)) {
                    movieIdByTopSimilar.add(movieID);
                }
            }
        }

        for (String j : movieIdByTopSimilar) {
            if (f.satisfies(j)) {
                double ave = 0;
                List<Rating> simiList = getSimilarities(raterId);
                int count = 0;
                double total = 0;
                double simiweighttotal = 0;
                for (int i = 0; i < numSimilarRaters; i++) {
                    double rating = RaterDatabase.getRater(simiList.get(i).getItem()).getRating(j);
                    if (rating != -1) {
                        count++;
                        total += rating * simiList.get(i).getValue();
                        simiweighttotal += simiList.get(i).getValue();
                    }
                }
                if (count >= minimalRaters)
                    ave = total / simiweighttotal;
                if (ave > 0)
                    ratingList.add(new Rating(j, ave));
            }
        }
        Collections.sort(ratingList);
        Collections.reverse(ratingList);

        return ratingList;
    }

    public static void main(String[] args) {
        MovieDatabase.initialize("ratedmovies_short.csv");
        RaterDatabase.initialize("ratings_short.csv");
        FourthRating sr = new FourthRating();
        logger.info("---------------test-------------");
        logger.info(sr.getSimilarRatings("2", 3, 0));

    }
}
