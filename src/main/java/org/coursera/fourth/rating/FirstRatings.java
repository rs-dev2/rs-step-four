package org.coursera.fourth.rating;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.coursera.entities.Movie;
import org.coursera.rater.Rater;
import org.coursera.rater.impl.CommonRaterImpl;
import org.coursera.fourth.utils.CsvParserUtils;
import org.coursera.fourth.utils.ResourceFileUtils;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Hello world!
 */
public class FirstRatings {
    private static final Logger logger = LogManager.getLogger();

    public List<Movie> loadMovies(String fileName) {
        try (Reader reader = ResourceFileUtils.createReaderFromResourceFile(fileName);
             CSVParser csvParser = CsvParserUtils.createDefaultCsvParser(reader)) {
            List<Movie> movies = new ArrayList<>();

            csvParser.forEach(csvRecord -> {
                String id = csvRecord.get("id").trim();
                String title = csvRecord.get("title").trim();
                int year = Integer.parseInt(csvRecord.get("year").trim());
                String country = csvRecord.get("country");
                String genre = csvRecord.get("genre");
                String director = csvRecord.get("director");
                int minutes = Integer.parseInt(csvRecord.get("minutes"));
                String poster = csvRecord.get("poster");
                Movie movie = new Movie(id, title, year, genre, director, country, poster, minutes);
                movies.add(movie);
            });
            return movies;
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage(), e);
            return Collections.emptyList();
        }
    }

    public void testLoadMovies() {
        List<Movie> movies = loadMovies("data/ratedmovies_short.csv");
        logger.info("The size of movie list is = {}", movies.size());
        Map<String, List<Movie>> directorMap = new HashMap<>();
        for (Movie movie : movies) {
            String director = movie.getDirector();
            String[] directors = director.split(", ");

            for (String j : directors) {
                if (!directorMap.containsKey(j)) {
                    ArrayList<Movie> a = new ArrayList<>();
                    a.add(movie);
                    directorMap.put(j, a);
                } else {
                    List<Movie> a = directorMap.get(j);
                    a.add(movie);
                    directorMap.put(j, a);
                }
            }
        }

        int maxNumMovieByDirector = 0;
        for (Map.Entry<String, List<Movie>> entry : directorMap.entrySet()) {
            if (directorMap.get(entry.getKey()).size() > maxNumMovieByDirector) {
                maxNumMovieByDirector = directorMap.get(entry.getKey()).size();
            }
        }
        for (Map.Entry<String, List<Movie>> entry : directorMap.entrySet()) {
            if (directorMap.get(entry.getKey()).size() == maxNumMovieByDirector) {
                logger.info("The director who produce the most movie is : {} : {}", maxNumMovieByDirector, entry.getKey());
            }
        }
    }

    public List<Rater> loadRaters(String fileName) {
        List<Rater> raters = new ArrayList<>();
        try (Reader reader = ResourceFileUtils.createReaderFromResourceFile(fileName);
             CSVParser csvParser = CsvParserUtils.createDefaultCsvParser(reader)) {
            for (CSVRecord csvRecord : csvParser) {
                String raterId = csvRecord.get("rater_id");
                String movieId = csvRecord.get("movie_id");
                double rating = Double.parseDouble(csvRecord.get("rating"));
                int count = 0;
                for (Rater rater : raters) {
                    if (rater.getId().contains(raterId)) {
                        rater.addRating(movieId, rating);
                        count++;
                        break;
                    }
                }
                if (count == 0) {
                    CommonRaterImpl m = new CommonRaterImpl(raterId);
                    m.addRating(movieId, rating);
                    raters.add(m);
                }
            }
            return raters;
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage(), e);
            return Collections.emptyList();
        }
    }

    public void testLoadRaters() {
        List<Rater> raters = loadRaters("data/ratings_short.csv");
        logger.info("The size of rater list is = {}", raters.size());
        for (Rater rater : raters) {
            if (rater.getId().equals("2")) {
                logger.info("USER # {} : {} ratings", rater.getId(), rater.numRatings());
                List<String> rating = rater.getItemsRated();
                for (String rat : rating) {
                    logger.info("MovieId: {} {} rating", rat, rater.getRating(rat));
                }
            }
        }
        int max = 0;
        for (Rater rater : raters) {
            if (rater.numRatings() > max) {
                max = rater.numRatings();
            }
        }
        for (Rater rater : raters) {
            if (rater.numRatings() == max) {
                logger.info("The maximum rate is from USER # {} : {} ratings", rater.getId(), rater.numRatings());
            }
        }
        int count = 0;
        String movieId = "1798709";
        for (Rater rater : raters) {
            List<String> rating = rater.getItemsRated();
            if (rating.contains(movieId)) {
                count++;
                logger.info("{} : id = {} {}", count, rater.getId(), rating);
            }
        }
        logger.info("The total # of {} that been rated is {}", movieId, count);
        ArrayList<String> differentMovie = new ArrayList<>();
        for (Rater rater : raters) {
            List<String> rating = rater.getItemsRated();
            for (String j : rating) {
                if (!differentMovie.contains(j)) {
                    differentMovie.add(j);
                }
            }
        }
        logger.info("The total # of movie is {}", differentMovie.size());
    }

    public static void main(String[] args) {
        FirstRatings a = new FirstRatings();
        logger.info("-------------------MOVIES-------------------");
        a.testLoadMovies();
        logger.info("");
        logger.info("");
        logger.info("-------------------RATERS-------------------");
        a.testLoadRaters();
    }
}
