package org.coursera.fourth.rating;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.coursera.entities.Movie;
import org.coursera.entities.Rating;
import org.coursera.rater.Rater;

import java.util.ArrayList;
import java.util.List;

public class SecondRatings {
    private static final Logger logger = LogManager.getLogger();

    private final List<Movie> movies;
    private final List<Rater> raters;

    public SecondRatings(String movieFile, String ratingFile) {
        FirstRatings firstRatings = new FirstRatings();
        this.movies = firstRatings.loadMovies(movieFile);
        this.raters = firstRatings.loadRaters(ratingFile);
    }

    public SecondRatings() {
        this("ratedmoviesfull.csv", "ratings.csv");
    }

    public int getMovieSize() {
        return movies.size();
    }

    public int getRaterSize() {
        return raters.size();
    }

    private double getAverageByID(String movieID, int minimalRaters) {
        int count = 0;
        double total = 0;
        for (Rater i : raters) {
            double rating = i.getRating(movieID);
            if (rating != -1) {
                count++;
                total += rating;
            }
        }
        if (count >= minimalRaters) {
            return count == 0 ? 0.0 : total / count;
        }
        return 0.0;
    }

    public List<Rating> getAverageRatings(int minimalRaters) {
        List<Rating> ratingList = new ArrayList<>();
        for (Movie i : movies) {
            double ave = getAverageByID(i.getId(), minimalRaters);
            if (ave > 0)
                ratingList.add(new Rating(i.getId(), ave));//item is string id?
        }
        return ratingList;
    }

    public String getTitle(String movieID) {
        for (Movie i : movies) {
            if (i.getId().equals(movieID)) {
                return i.getTitle();
            }
        }
        return "The Movie ID was not found!";
    }

    public String getID(String title) {
        for (Movie i : movies) {
            if (i.getTitle().equals(title)) {
                return i.getId();
            }
        }
        return "NO SUCH TITLE";
    }

    public static void main(String[] args) {
        SecondRatings sr = new SecondRatings("data/ratedmovies_short.csv", "data/ratings_short.csv");
        logger.info(sr.getAverageByID("0790636", 2));
        logger.info("---------------test-------------");
        logger.info(sr.getAverageRatings(2));
        //[[6414, 0.0], [68646, 0.0], [113277, 0.0], [1798709, 8.25], [790636, 0.0]]
    }
}
