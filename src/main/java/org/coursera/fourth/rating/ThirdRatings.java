package org.coursera.fourth.rating;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.coursera.entities.Rating;
import org.coursera.fourth.db.MovieDatabase;
import org.coursera.fourth.filter.Filter;
import org.coursera.fourth.filter.impl.TrueFilter;
import org.coursera.rater.Rater;

import java.util.ArrayList;
import java.util.List;

public class ThirdRatings {
    private static final Logger logger = LogManager.getLogger();
    private final List<Rater> raters;

    public ThirdRatings() {
        this("ratings.csv");
    }

    public ThirdRatings(String ratingFile) {
        FirstRatings a = new FirstRatings();
        raters = a.loadRaters(ratingFile);
    }

    public int getRaterSize() {
        return raters.size();
    }

    private double getAverageById(String movieId, int minimalRaters) {
        int count = 0;
        double total = 0;
        for (Rater i : raters) {
            double rating = i.getRating(movieId);
            if (rating != -1) {
                count++;
                total += rating;
            }
        }
        String s = String.format("Movie ID : Count : Total : Rating = %-10s%-5d%-7.2f%-7.2f", movieId, count, total, total / count);
        logger.info(s);
        if (count >= minimalRaters) return total / count;
        return 0.0;
    }

    public List<Rating> getAverageRatings(int minimalRaters) {
        List<Rating> ratingList = new ArrayList<>();
        Filter trueFilter = new TrueFilter();
        for (String movieId : MovieDatabase.filterBy(trueFilter)) {
            double ave = getAverageById(movieId, minimalRaters);
            if (ave > 0)
                ratingList.add(new Rating(movieId, ave));
        }
        return ratingList;
    }

    public List<Rating> getAverageRatingsByFilter(int minimalRaters, Filter f) {
        List<Rating> ratingList = new ArrayList<>();
        Filter trueFilter = new TrueFilter();
        List<String> movieId = MovieDatabase.filterBy(trueFilter);

        for (String i : movieId) {
            if (f.satisfies(i)) {
                double ave = getAverageById(i, minimalRaters);
                if (ave > 0)
                    ratingList.add(new Rating(i, ave));
            }
        }
        return ratingList;
    }

    public static void main(String[] args) {
        ThirdRatings sr = new ThirdRatings("data/ratings_short.csv");
        logger.info("---------------test-------------");
        logger.info(sr.getAverageRatings(2));
    }
}
