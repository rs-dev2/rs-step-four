package org.coursera.fourth.filter.impl;

import org.coursera.fourth.db.MovieDatabase;
import org.coursera.fourth.filter.Filter;

public class DirectorsFilter implements Filter {
    private final String directors;

    public DirectorsFilter(String directors) {
        this.directors = directors;
    }

    @Override
    public boolean satisfies(String id) {
        String[] dir = directors.split(",");
        for (String i : dir) {
            if (MovieDatabase.getDirector(id).contains(i)) {
                return true;
            }
        }
        return false;
    }
}
