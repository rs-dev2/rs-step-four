package org.coursera.fourth.filter.impl;

import org.coursera.fourth.filter.Filter;

public class TrueFilter implements Filter {
    @Override
    public boolean satisfies(String id) {
        return true;
    }
    
}
