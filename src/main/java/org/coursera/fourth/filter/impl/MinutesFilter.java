package org.coursera.fourth.filter.impl;

import org.coursera.fourth.db.MovieDatabase;
import org.coursera.fourth.filter.Filter;

public class MinutesFilter implements Filter {
    private final int min;
    private final int max;
    
    
    public MinutesFilter(int min, int max) {
        this.min = min;
        this.max = max;
    }
    
    @Override
    public boolean satisfies(String id) {
        return MovieDatabase.getMinutes(id) >= min && MovieDatabase.getMinutes(id) <= max;
    }
}
