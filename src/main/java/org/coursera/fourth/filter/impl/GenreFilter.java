package org.coursera.fourth.filter.impl;

import org.coursera.fourth.db.MovieDatabase;
import org.coursera.fourth.filter.Filter;

public class GenreFilter implements Filter {
    private String genre;
    
    public GenreFilter(String genre) {
        this.genre = genre;
    }
    
    @Override
    public boolean satisfies(String id) {
        return MovieDatabase.getGenres(id).contains(genre);
    }
}
