package org.coursera.fourth.filter;

public interface Filter {
    boolean satisfies(String id);
}
