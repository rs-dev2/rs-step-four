package org.coursera.fourth.runner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.coursera.entities.Rating;
import org.coursera.fourth.db.MovieDatabase;
import org.coursera.fourth.filter.Filter;
import org.coursera.fourth.filter.impl.AllFilters;
import org.coursera.fourth.filter.impl.DirectorsFilter;
import org.coursera.fourth.filter.impl.GenreFilter;
import org.coursera.fourth.filter.impl.MinutesFilter;
import org.coursera.fourth.filter.impl.YearAfterFilter;
import org.coursera.fourth.rating.ThirdRatings;

import java.util.Collections;
import java.util.List;

public class MovieRunnerWithFilters {
    private static final Logger logger = LogManager.getLogger();

    public void printAverageRatings() {
        ThirdRatings tr = new ThirdRatings("data/ratings_short.csv");
        MovieDatabase.initialize("ratedmovies_short.csv");
        logger.info("Movie size (# of movie in list) : {}", MovieDatabase.size());
        logger.info("Rater size (# of ppl who rates) : {}", tr.getRaterSize());
        List<Rating> ratingList = tr.getAverageRatings(1);
        logger.info("Found ratings for movies : {}", ratingList.size());
        Collections.sort(ratingList);
        for (Rating i : ratingList) {
            String s = String.format("%-10.2f%s", i.getValue(), MovieDatabase.getTitle(i.getItem()));
            logger.info(s);
        }
    }

    public void getAverageRatingOneMovie() {
        ThirdRatings sr = new ThirdRatings("data/ratings_short.csv");
        List<Rating> ratingList = sr.getAverageRatings(1);
        String movieTitle = "The Godfather";
        boolean exist = false;
        for (Rating i : ratingList) {
            if (MovieDatabase.getTitle(i.getItem()).equals(movieTitle)) {
                String s = String.format("%-10.2f%s", i.getValue(), movieTitle);
                logger.info(s);
                exist = true;
            }
        }
        if (!exist) {
            logger.info("MOVIE TITLE NOT FOUND!");
        }
    }

    public void printAverageRatingsByYear() {
        ThirdRatings tr3 = new ThirdRatings("data/ratings_short.csv");
        MovieDatabase.initialize("ratedmovies_short.csv");
        logger.info("Movie size (# of movie in list) : {}", MovieDatabase.size());
        logger.info("Rater size (# of ppl who rates) : {}", tr3.getRaterSize());
        List<Rating> ratingList = tr3.getAverageRatingsByFilter(1, new YearAfterFilter(2000));
        logger.info("Found ratings for movies : {}", ratingList.size());
        Collections.sort(ratingList);
        for (Rating i : ratingList) {
            String s = String.format("%-10.2f%-10s%-5s", i.getValue(), MovieDatabase.getYear(i.getItem()), MovieDatabase.getTitle(i.getItem()));
            logger.info(s);
        }
    }

    public void printAverageRatingsByGenre() {
        ThirdRatings thirdRatings = new ThirdRatings("data/ratings_short.csv");
        MovieDatabase.initialize("ratedmovies_short.csv");
        logger.info("Movie size (# of movie in list) : {}", MovieDatabase.size());
        logger.info("Rater size (# of ppl who rates) : {}", thirdRatings.getRaterSize());
        List<Rating> ratings = thirdRatings.getAverageRatingsByFilter(1, new GenreFilter("Crime"));
        logger.info("Found ratings for movies : {}", ratings.size());
        Collections.sort(ratings);
        for (Rating i : ratings) {
            String s = String.format("%-10.2f%-16s%-5s", i.getValue(), MovieDatabase.getTitle(i.getItem()), MovieDatabase.getGenres(i.getItem()));
            logger.info(s);
        }
    }

    public void printAverageRatingsByMinutes() {
        ThirdRatings tr5 = new ThirdRatings("data/ratings_short.csv");
        MovieDatabase.initialize("ratedmovies_short.csv");
        logger.info("Movie size (# of movie in list) : {}", MovieDatabase.size());
        logger.info("Rater size (# of ppl who rates) : {}", tr5.getRaterSize());
        List<Rating> ratingList = tr5.getAverageRatingsByFilter(1, new MinutesFilter(110, 170));
        logger.info("Found ratings for movies : {}", ratingList.size());
        Collections.sort(ratingList);
        for (Rating i : ratingList) {
            String s = String.format("%-10.2f%-16s%-5s", i.getValue(), MovieDatabase.getMinutes(i.getItem()), MovieDatabase.getTitle(i.getItem()));
            logger.info(s);
        }
    }

    public void printAverageRatingsByDirectors() {
        ThirdRatings thirdRatings = new ThirdRatings("data/ratings_short.csv");
        MovieDatabase.initialize("ratedmovies_short.csv");
        logger.info("Movie size (# of movie in list) : {}", MovieDatabase.size());
        logger.info("Rater size (# of ppl who rates) : {}", thirdRatings.getRaterSize());
        Filter d = new DirectorsFilter("Charles Chaplin,Michael Mann,Spike Jonze");
        List<Rating> ratingList = thirdRatings.getAverageRatingsByFilter(1, d);
        logger.info("Found ratings for movies : {}", ratingList.size());
        Collections.sort(ratingList);
        for (Rating i : ratingList) {
            String s = String.format("%-10.2f%-16s%-5s", i.getValue(), MovieDatabase.getTitle(i.getItem()), MovieDatabase.getDirector(i.getItem()));
            logger.info(s);
        }
    }

    public void printAverageRatingsByYearAfterAndGenre() {
        ThirdRatings tr5 = new ThirdRatings("data/ratings_short.csv");//do i need put filename here?
        MovieDatabase.initialize("ratedmovies_short.csv");
        logger.info("Movie size (# of movie in list) : {}", MovieDatabase.size());// need initialize first.
        logger.info("Rater size (# of ppl who rates) : {}", tr5.getRaterSize());
        //why here must use AllFilters instead of Filter?
        AllFilters all = new AllFilters();
        all.addFilter(new GenreFilter("Romance"));
        all.addFilter(new YearAfterFilter(1980));

        List<Rating> ratingList = tr5.getAverageRatingsByFilter(1, all);
        logger.info("Found ratings for movies : {}", ratingList.size());
        Collections.sort(ratingList);
        for (Rating rating : ratingList) {
            String s = String.format("%-10.2f%-10d%-16s%-5s", rating.getValue(), MovieDatabase.getYear(rating.getItem()), MovieDatabase.getTitle(rating.getItem()), MovieDatabase.getGenres(rating.getItem()));
            logger.info(s);
        }
    }

    public void printAverageRatingsByDirectorsAndMinutes() {
        ThirdRatings tr5 = new ThirdRatings("data/ratings_short.csv");//do i need put filename here?
        MovieDatabase.initialize("ratedmovies_short.csv");
        logger.info("Movie size (# of movie in list) : {}", MovieDatabase.size());// need initialize first.
        logger.info("Rater size (# of ppl who rates) : {}", tr5.getRaterSize());
        //why here must use AllFilters instead of Filter?
        AllFilters all = new AllFilters();
        all.addFilter(new MinutesFilter(30, 170));
        all.addFilter(new DirectorsFilter("Spike Jonze,Michael Mann,Charles Chaplin,Francis Ford Coppola"));

        List<Rating> ratingList = tr5.getAverageRatingsByFilter(1, all);
        logger.info("Found ratings for movies : {}", ratingList.size());
        Collections.sort(ratingList);
        for (Rating rating : ratingList) {
            String s = String.format("%-10.2fTime:%-10s%-16s%-5s", rating.getValue(), MovieDatabase.getMinutes(rating.getItem()), MovieDatabase.getTitle(rating.getItem()), MovieDatabase.getDirector(rating.getItem()));
            logger.info(s);
        }
    }

    public static void main(String[] args) {
        MovieRunnerWithFilters mra = new MovieRunnerWithFilters();
        logger.info("---------------Test: printAverageRatings()----------------");
        mra.printAverageRatings();
        logger.info("---------------Test: getAverageRatingOneMovie() ----------------");
        mra.getAverageRatingOneMovie();
        logger.info("---------------Test: printAverageRatingsByYear() ----------------");
        mra.printAverageRatingsByYear();
        logger.info("---------------Test: printAverageRatingsByGenre() ----------------");
        mra.printAverageRatingsByGenre();
        logger.info("---------------Test: printAverageRatingsByMinutes() ----------------");
        mra.printAverageRatingsByMinutes();
        logger.info("---------------Test: printAverageRatingsByDirectors() ----------------");
        mra.printAverageRatingsByDirectors();
        logger.info("---------------Test: printAverageRatingsByYearAfterAndGenre() ----------------");
        mra.printAverageRatingsByYearAfterAndGenre();
        logger.info("---------------Test: printAverageRatingsByDirectorsAndMinutes() ----------------");
        mra.printAverageRatingsByDirectorsAndMinutes();

    }
}
