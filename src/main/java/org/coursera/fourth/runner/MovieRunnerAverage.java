package org.coursera.fourth.runner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.coursera.entities.Rating;
import org.coursera.fourth.rating.SecondRatings;

import java.util.Collections;
import java.util.List;

public class MovieRunnerAverage {
    private static final Logger logger = LogManager.getLogger();

    public void printAverageRatings() {
        SecondRatings secondRatings = new SecondRatings("data/ratedmovies_short.csv", "data/ratings_short.csv");
        logger.info("Movie size = {}", secondRatings.getMovieSize());
        logger.info("Rater size = {}", secondRatings.getRaterSize());
        List<Rating> ratingList = secondRatings.getAverageRatings(2);
        Collections.sort(ratingList);
        for (Rating i : ratingList) {
            String s = String.format("%-10.2f%s", i.getValue(), secondRatings.getTitle(i.getItem()));
            logger.info(s);
        }
    }

    public void getAverageRatingOneMovie() {
        SecondRatings sr = new SecondRatings("data/ratedmovies_short.csv", "data/ratings_short.csv");
        List<Rating> ratingList = sr.getAverageRatings(2);
        String movieTitle = "The Godfather";
        for (Rating i : ratingList) {
            if (sr.getTitle(i.getItem()).equals(movieTitle)) {
                String s = String.format("%-10.2f%s%n", i.getValue(), sr.getTitle(i.getItem()));
                logger.info(s);
            }
        }
    }

    public static void main(String[] args) {
        MovieRunnerAverage mra = new MovieRunnerAverage();
        logger.info("---------------print test----------------");
        mra.printAverageRatings();
        logger.info("---------------get average rating one movie test----------------");
        mra.getAverageRatingOneMovie();
    }
}
