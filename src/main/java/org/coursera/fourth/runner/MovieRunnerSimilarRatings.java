package org.coursera.fourth.runner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.coursera.entities.Rating;
import org.coursera.fourth.db.MovieDatabase;
import org.coursera.fourth.db.RaterDatabase;
import org.coursera.fourth.filter.impl.AllFilters;
import org.coursera.fourth.filter.impl.DirectorsFilter;
import org.coursera.fourth.filter.impl.GenreFilter;
import org.coursera.fourth.filter.impl.MinutesFilter;
import org.coursera.fourth.filter.impl.YearAfterFilter;
import org.coursera.fourth.rating.FourthRating;

import java.util.List;

public class MovieRunnerSimilarRatings {
    private static final Logger logger = LogManager.getLogger();

    public void printSimilarRatings() {
        FourthRating tr = new FourthRating();
        List<Rating> ratings = tr.getSimilarRatings("65", 20, 5);
        logger.info("Found ratings for movies : {}", ratings.size());
        for (int i = 0; i < 3; i++) {
            String format = String.format("%-10.2f%s", ratings.get(i).getValue(), MovieDatabase.getTitle(ratings.get(i).getItem()));
            logger.info(format);
        }
    }

    public void printSimilarRatingsByGenre() {
        FourthRating fourthRating = new FourthRating();
        List<Rating> ratings = fourthRating.getSimilarRatingsByFilter("65", 20, 5, new GenreFilter("Action"));
        logger.info("Found ratings for movies : {}", ratings.size());
        for (int i = 0; i < 3; i++) {
            String format = String.format("%-10.2f%-16s%-5s", ratings.get(i).getValue(), MovieDatabase.getTitle(ratings.get(i).getItem()), MovieDatabase.getGenres(ratings.get(i).getItem()));
            logger.info(format);
        }

    }

    public void printSimilarRatingsByDirector() {
        FourthRating fourthRating = new FourthRating();//do i need put filename here?
        List<Rating> ratings = fourthRating.getSimilarRatingsByFilter("1034", 10, 3, new DirectorsFilter("Clint Eastwood,Sydney Pollack,David Cronenberg,Oliver Stone"));
        logger.info("Found ratings for movies : {}", ratings.size());
        int printNum = ratings.size();
        if (printNum >= 3) printNum = 3;
        for (int i = 0; i < printNum; i++) {
            String format = String.format("%-10.2f%-16s%-5s", ratings.get(i).getValue(), MovieDatabase.getTitle(ratings.get(i).getItem()), MovieDatabase.getDirector(ratings.get(i).getItem()));
            logger.info(format);
        }

    }

    public void printSimilarRatingsByGenreAndMinutes() {
        FourthRating tr4 = new FourthRating();
        AllFilters a = new AllFilters();
        a.addFilter(new GenreFilter("Adventure"));
        a.addFilter(new MinutesFilter(100, 200));
        List<Rating> ratingList = tr4.getSimilarRatingsByFilter("65", 10, 5, a);
        logger.info("Found ratings for movies : {}", ratingList.size());
        int printNum = ratingList.size();
        if (printNum >= 3) printNum = 3;
        for (int i = 0; i < printNum; i++) {
            String format = String.format("%-10.2f%-5d%-16s%-5s", ratingList.get(i).getValue(), MovieDatabase.getMinutes(ratingList.get(i).getItem()),
                    MovieDatabase.getTitle(ratingList.get(i).getItem()), MovieDatabase.getGenres(ratingList.get(i).getItem()));
            logger.info(format);
        }

    }

    public void printSimilarRatingsByYearAfterAndMinutes() {
        FourthRating tr4 = new FourthRating();//do i need put filename here?
        AllFilters a = new AllFilters();
        a.addFilter(new YearAfterFilter(2000));
        a.addFilter(new MinutesFilter(80, 100));
        List<Rating> ratingList = tr4.getSimilarRatingsByFilter("65", 10, 5, a);
        logger.info("Found ratings for movies : {}", ratingList.size());
        int printNum = ratingList.size();
        if (printNum >= 3) printNum = 3;
        for (int i = 0; i < printNum; i++) {
            String s = String.format("%-10.2f%-5s", ratingList.get(i).getValue(), MovieDatabase.getMovie(ratingList.get(i).getItem()).toString());
            logger.info(s);
        }
    }

    public static void main(String[] args) {
        MovieRunnerSimilarRatings movieRunnerSimilarRatings = new MovieRunnerSimilarRatings();
        logger.info("-----------The FOLLOWING RESULTS are Algorithm by DUKE -------------");
        MovieDatabase.initialize("ratedmoviesfull.csv");
        RaterDatabase.initialize("ratings.csv");
        logger.info("Movie size (# of movie in list) : {}", MovieDatabase.size());
        logger.info("Rater size (# of ppl who rates) : {}", RaterDatabase.size());
        logger.info("---------------Test: printSimilarRatings()----------------");
        double start1 = System.nanoTime();
        movieRunnerSimilarRatings.printSimilarRatings();
        double duration1 = (System.nanoTime() - start1) / 1000000000;
        logger.info("---------------Duration =  {}s-------------", duration1);

        logger.info("---------------Test: printSimilarRatingsByGenre()----------------");
        double start2 = System.nanoTime();
        movieRunnerSimilarRatings.printSimilarRatingsByGenre();
        double duration2 = (System.nanoTime() - start2) / 1000000000;
        logger.info("---------------Duration = {}s-------------", duration2);
        logger.info("---------------Test: printSimilarRatingsDirector()----------------");
        double start3 = System.nanoTime();
        movieRunnerSimilarRatings.printSimilarRatingsByDirector();
        double duration3 = (System.nanoTime() - start3) / 1000000000;
        logger.info("---------------Duration = {}s-------------", duration3);
        logger.info("---------------Test: printSimilarRatingsByGenreAndMinutes()----------------");
        double start4 = System.nanoTime();
        movieRunnerSimilarRatings.printSimilarRatingsByGenreAndMinutes();
        double duration4 = (System.nanoTime() - start4) / 1000000000;
        logger.info("---------------Duration =  {}s-------------", duration4);

        logger.info("---------------Test: printSimilarRatingsByYearAfterAndMinutes()----------------");
        double start5 = System.nanoTime();
        movieRunnerSimilarRatings.printSimilarRatingsByYearAfterAndMinutes();
        double duration5 = (System.nanoTime() - start5) / 1000000000;
        logger.info("---------------Duration = {}s-------------", duration5);

    }
}
