package org.coursera.fourth.db;

import org.coursera.entities.Movie;
import org.coursera.fourth.filter.Filter;
import org.coursera.fourth.rating.FirstRatings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MovieDatabase {
    private static Map<String, Movie> ourMovies;

    private MovieDatabase() {

    }

    public static void initialize(String moviefile) {
        if (ourMovies == null) {
            ourMovies = new HashMap<>();
            loadMovies("data/" + moviefile);
        }
    }

    private static void initialize() {
        if (ourMovies == null) {
            ourMovies = new HashMap<>();
            loadMovies("data/ratedmoviesfull.csv");
        }
    }

    private static void loadMovies(String filename) {
        FirstRatings firstRatings = new FirstRatings();
        List<Movie> movies = firstRatings.loadMovies(filename);
        for (Movie movie : movies) {
            ourMovies.put(movie.getId(), movie);
        }
    }

    public static boolean containsID(String id) {
        initialize();
        return ourMovies.containsKey(id);
    }

    public static int getYear(String id) {
        initialize();
        return ourMovies.get(id).getYear();
    }

    public static String getGenres(String id) {
        initialize();
        return ourMovies.get(id).getGenres();
    }

    public static String getTitle(String id) {
        initialize();
        return ourMovies.get(id).getTitle();
    }

    public static Movie getMovie(String id) {
        initialize();
        return ourMovies.get(id);
    }

    public static String getPoster(String id) {
        initialize();
        return ourMovies.get(id).getPoster();
    }

    public static int getMinutes(String id) {
        initialize();
        return ourMovies.get(id).getMinutes();
    }

    public static String getCountry(String id) {
        initialize();
        return ourMovies.get(id).getCountry();
    }

    public static String getDirector(String id) {
        initialize();
        return ourMovies.get(id).getDirector();
    }

    public static int size() {
        return ourMovies.size();
    }

    public static List<String> filterBy(Filter filter) {
        initialize();
        List<String> moviesId = new ArrayList<>();
        for (String id : ourMovies.keySet()) {
            if (filter.satisfies(id)) {
                moviesId.add(id);
            }
        }
        return moviesId;
    }
}
