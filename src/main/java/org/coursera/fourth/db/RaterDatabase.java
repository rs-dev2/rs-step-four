package org.coursera.fourth.db;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.coursera.CsvParserUtils;
import org.coursera.ResourceFileUtils;
import org.coursera.fourth.rater.EfficientRater;
import org.coursera.rater.Rater;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RaterDatabase {
    private static final Logger logger = LogManager.getLogger();
    private static Map<String, Rater> ourRaters;

    private static void initialize() {
        if (ourRaters == null) {
            ourRaters = new HashMap<>();
        }
    }

    public static void initialize(String filename) {
        if (ourRaters == null) {
            ourRaters = new HashMap<>();
            addRatings("data/" + filename);
        }
    }

    public static void addRatings(String filename) {
        initialize();
        try (Reader reader = ResourceFileUtils.createReaderFromResourceFile(filename);
             CSVParser csvParser = CsvParserUtils.createDefaultCsvParser(reader)) {
            for (CSVRecord rec : csvParser) {
                String id = rec.get("rater_id");
                String item = rec.get("movie_id");
                String rating = rec.get("rating");
                addRaterRating(id, item, Double.parseDouble(rating));
            }
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage(), e);
        }

    }

    public static void addRaterRating(String raterId, String movieId, double rating) {
        initialize();
        Rater rater;
        if (ourRaters.containsKey(raterId)) {
            rater = ourRaters.get(raterId);
        } else {
            rater = new EfficientRater(raterId);
            ourRaters.put(raterId, rater);
        }
        rater.addRating(movieId, rating);
    }

    public static Rater getRater(String id) {
        initialize();

        return ourRaters.get(id);
    }

    public static List<Rater> getRaters() {
        initialize();
        return new ArrayList<>(ourRaters.values());
    }

    public static int size() {
        return ourRaters.size();
    }


}
