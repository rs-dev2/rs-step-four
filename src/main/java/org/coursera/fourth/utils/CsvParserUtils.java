package org.coursera.fourth.utils;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.IOException;
import java.io.Reader;

public class CsvParserUtils {
    public static CSVParser createDefaultCsvParser(Reader reader) throws IOException {
        return CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(reader);
    }
}
